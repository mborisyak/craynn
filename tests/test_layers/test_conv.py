import tensorflow as tf
from craynn import layers

def test_conv():
  input = layers.InputLayer(shape=(None, 1, 32, 32))
  l1 = layers.conv(32)(input)
  l2 = layers.conv(2)(l1)

  @tf.function
  def f(X):
    return layers.get_output(l2, substitutes={ input : X })

  y = f(tf.random.normal(shape=(5, 1, 32, 32)))

  assert y.shape == (5, 2, 28, 28)