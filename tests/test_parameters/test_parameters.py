import numpy as np
import tensorflow as tf
from craynn.parameters import *

def test_parameters():
  W = ones_init(trainable=False)(shape=(32, 32), trainable=True)
  assert not W.properties['trainable']
  assert W().shape == (32, 32)
  assert np.allclose(W().numpy(), np.ones(shape=(32, 32)))

  W = glorot_normal_init(gain=1.0, trainable=False)(shape=(32, 32), trainable=True)
  assert W().shape == (32, 32)

  W1 = W().numpy()
  W.reset()
  W2 = W().numpy()

  assert np.any(W1 != W2)