import numpy as np
import tensorflow as tf
from craynn import *

def test_scaled():
  return
  W = decomposition(n=3)(shape=(32, 32), weights=True)
  sW = scaled(W)

  print('Initial sum:', np.sum(W().numpy()))

  initial_delta = np.abs(np.sum(W().numpy()) - 1)

  @tf.function
  def target():
    return (tf.reduce_sum(sW()) - 1) ** 2

  opt = tf_updates.adam(learning_rate=1e-2)(target, [augment() for augment in sW.get_augments()])

  for _ in range(1024):
    opt()

  print('Normalization, pre apply:', np.sum(sW().numpy()))

  print([scale() for scale in sW.get_augments()])

  sW.apply()

  print('Normalization, post apply:', np.sum(W().numpy()))
  assert np.abs(np.sum(W().numpy()) - 1) < initial_delta * 1e-2

def test_normalize():
  net = network((None, 4))(
    [ dense(32, W=glorot_normal_init(gain=0.5)), dense(16, W=glorot_normal_init(gain=0.22)) ],
    concat(),
    [dense(5, W=glorot_normal_init(gain=0.5)), dense(14, W=glorot_normal_init(gain=0.2))],
    concat(),
    dense(32, W=glorot_normal_init(gain=0.7)),
    dense(1, W=glorot_normal_init(gain=0.2)),
    flatten(1)
  )

  batch_f = tf.function(
    lambda : (
      tf.random.normal(shape=(32, 4)),
    )
  )

  predictions = np.concatenate([
    net(*batch_f())
  ],axis=0)

  print(np.mean(predictions), '+-', np.std(predictions))

  train.normalize_weights(
    net, batch_f
  )

  predictions = np.concatenate([
    net(*batch_f())
  ], axis=0)

  print(np.mean(predictions), '+-', np.std(predictions))
