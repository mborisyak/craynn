from craynn.subnetworks import with_inputs

def test_selector():
  assert with_inputs(0, 1)(
    lambda *xs: [x + 1 for x in xs]
  )(1, 2, 3, 4) == [2, 3, 3, 4]

  assert with_inputs(1, 3)(
    lambda *xs: [x + 1 for x in xs]
  )(1, 2, 3, 4) == [1, 3, 3, 5]

  assert with_inputs(1, 2)(
    lambda *xs: [ x + 1 for x in xs ] + [ sum(xs) ]
  )(1, 2, 3, 4) == [1, 3, 4, 4, 5]

  assert with_inputs(0, 1)(
    lambda *xs: sum(xs)
  )(1, 2, 3, 4) == [3, 3, 4]