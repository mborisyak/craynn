import numpy as np
import tensorflow as tf
from craynn import *

def test_variable_dataset():
  data = np.repeat(np.arange(12), 5, axis=0).reshape((12, 5)).astype('float32')
  dataset = variable_dataset(data)

  def check_batch(f, min, max):
    for i in range(128):
      values, = f()
      assert np.allclose(np.min(values, axis=1), np.max(values, axis=1))
      assert np.min(values) >= min and np.max(values) <= max

  check_batch(dataset.batch_f(32), 0, 11)
  check_batch(dataset.subset[:].batch_f(32), 0, 11)
  check_batch(dataset.subset[1:].batch_f(32), 1, 11)
  check_batch(dataset.subset[:-1].batch_f(32), 0, 10)
  check_batch(dataset.subset[2:-2].batch_f(32), 2, 9)

  dataset.assign(
    np.repeat(np.arange(9), 5, axis=0).reshape((9, 5)).astype('float32')
  )

  check_batch(dataset.batch_f(32), 0, 8)
  check_batch(dataset.subset[:].batch_f(32), 0, 8)
  check_batch(dataset.subset[1:].batch_f(32), 1, 8)
  check_batch(dataset.subset[:-1].batch_f(32), 0, 7)
  check_batch(dataset.subset[2:-2].batch_f(32), 2, 6)