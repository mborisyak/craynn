import os
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

import torch

from craynn.updates import *

here = os.path.dirname(__file__)

def get_tf_target():
  Ws = [
    tf.Variable(initial_value=tf.ones(shape=(32, 32)), dtype=tf.float32, trainable=True)
    for _ in range(32)
  ]

  target = lambda: sum([tf.reduce_sum(W ** 2) for W in Ws])
  return Ws, target

def get_torch_target():
  Ws = [
    torch.tensor(np.ones(shape=(32, 32)), dtype=torch.float32, device='cuda:0', requires_grad=True)
    for _ in range(32)
  ]

  target = lambda: sum([torch.sum(W ** 2) for W in Ws])

  return Ws, target

def eval_torch(n_iter, optimizer):
  import time

  fs = np.zeros(n_iter + 1)

  Ws, target = get_torch_target()

  optimizer = optimizer(Ws)

  optimizer.zero_grad()
  t = target()
  t.backward()
  optimizer.step()

  start_time = time.time()
  for i in range(n_iter + 1):
    optimizer.zero_grad()
    t = target()
    fs[i] = t.item()
    t.backward()
    optimizer.step()
  delta_t = time.time() - start_time

  return (delta_t, fs[1:])

def eval_craynn(n_iter, optimizer):
  import time

  Ws, target = get_tf_target()

  fs = np.zeros(n_iter)

  opt = optimizer(target, Ws)

  opt()

  start_time = time.time()
  for i in range(n_iter):
    fs[i] = opt()
  delta_t = time.time() - start_time

  return (delta_t, fs)


def eval_keras(n_iter, optimizer):
  import time

  Ws, target = get_tf_target()

  fs = np.zeros(n_iter)

  @tf.function
  def step():
    with tf.GradientTape() as tape:
      value = target()
      grads = tape.gradient(value, Ws)
      optimizer.apply_gradients(zip(grads, Ws))

      return value

  ### warmup
  step()

  start_time = time.time()
  for i in range(n_iter):
    fs[i] = step().numpy()

  delta_t = time.time() - start_time

  return (delta_t, fs)

def compare(n_iter=1024, **kwargs):
  results = dict()

  for name, optimizer in kwargs.items():
    if 'torch' in name:
      results[name] = eval_torch(n_iter, optimizer)
    elif 'keras' in name:
      results[name] = eval_keras(n_iter, optimizer)
    else:
      results[name] = eval_craynn(n_iter, optimizer)

  return results

def plot(results, path):
  plt.figure()

  normalized_names = {}
  for name in results:
    if 'keras' in name:
      normalized_names[name] = name[len('keras') + 1:]
    elif 'torch' in name:
      normalized_names[name] = name[len('keras') + 1:]
    else:
      normalized_names[name] = name

  colors = {
    name : plt.cm.tab10(i)
    for i, name in enumerate(set(normalized_names.values()))
  }

  for i, (name, (t, fs)) in enumerate(results.items()):
    if 'keras' in name:
      ls = '--'
    elif 'torch' in name:
      ls = ':'
    else:
      ls = '-'

    plt.plot(
      fs,
      ls=ls, color=colors[normalized_names[name]],
      label='%s (%d iter/sec)' % (name, int(1024 / t))
    )

  plt.legend()
  # plt.yscale('log')
  plt.savefig(os.path.join(here, '%s.png' % (path, )))
  plt.close()

def test_sgd():
  results = compare(
    n_iter=256,

    sgd=sgd(1e-3),
    keras_sgd=tf.keras.optimizers.SGD(learning_rate=1e-3, decay=0.0),
    torch_sgd=lambda params: torch.optim.SGD(params, lr=1e-3),

    momentum=momentum(1e-3, rho=0.9),
    keras_momentum=tf.keras.optimizers.SGD(lr=1e-3, momentum=0.9, decay=0.0),
    torch_momentum=lambda params: torch.optim.SGD(params, lr=1e-3, momentum=0.9),

    rmsprop=rmsprop(1e-3, rho=0.9),
    keras_rmsprop=tf.keras.optimizers.RMSprop(learning_rate=1e-3, rho=0.9, decay=0.0, momentum=0.0),
    torch_rmsprop=lambda params: torch.optim.RMSprop(params, lr=1e-3, momentum=0.9),
  )
  plot(results, 'sgd')

def test_nesterov():
  results = compare(
    n_iter=256,
    nesterov=nesterov(1e-3, rho=0.95),
    keras_nesterov=tf.keras.optimizers.SGD(lr=1e-3, decay=0.0, momentum=0.95, nesterov=True),
  )

  plot(results, 'nesterov')

def test_adagrad():
  results = compare(
    n_iter=256,
    adagrad=adagrad(1e-2),
    keras_adagrad=tf.keras.optimizers.Adagrad(lr=1e-2, decay=0.0),
    torch_adagrad=lambda params: torch.optim.Adagrad(params, lr=1e-2),

    adam=tf_updates.adam(learning_rate=1e-2),
    keras_adam=tf.keras.optimizers.Adam(lr=1e-2, decay=0.0),
    torch_adam=lambda params: torch.optim.Adam(params, lr=1e-2),

    adamax=tf_updates.adamax(learning_rate=1e-2),
    keras_adamax=tf.keras.optimizers.Adamax(lr=1e-2, decay=0.0),
    torch_adamax=lambda params: torch.optim.Adamax(params, lr=1e-2),
  )

  plot(results, 'adagrad')

def test_adadelta():
  results = compare(
    n_iter=256,
    adadelta=adadelta(5e-3, rho=0.95),
    keras_adadelta=tf.keras.optimizers.Adadelta(lr=1e-1, decay=0.0, rho=0.95),
  )

  plot(results, 'adadelta')