craynn package
==============

Subpackages
-----------

.. toctree::

    craynn.layers
    craynn.meta
    craynn.networks
    craynn.objectives
    craynn.subnetworks
    craynn.updates
    craynn.utils
    craynn.viz

Submodules
----------

craynn.init module
------------------

.. automodule:: craynn.init
    :members:
    :undoc-members:
    :show-inheritance:

craynn.nonlinearities module
----------------------------

.. automodule:: craynn.nonlinearities
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: craynn
    :members:
    :undoc-members:
    :show-inheritance:
