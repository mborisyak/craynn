# CRAYNN

Yet Another toolkit for Neural Network slightly flavoured by Ultra-High Energy Cosmic Rays. 

## Philosophy

- it is just a collection of helpers for constructing tensorflow graphs;
- don't force any features;
- keep it modular and orthogonal:
  - e.g. any change in optimizers should not have any effect on the layers module. 

## TODO

- def helpers;
- common layers;
- dense layers;
- batch norm layer;
- conv layers;
- initialization;
- optimizers;
- network helper class.