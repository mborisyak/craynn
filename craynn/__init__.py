"""
CRAYNN is just another layer of abstraction for neural networks.

Important note: this package is poorly designed, unstable and lacks documentation.
"""

from .nonlinearities import *
from .regularization import *
from .parameters import *
from .updates import *
from .layers import *
from .subnetworks import *
from .networks import *

from . import train

from . import objectives

from . import utils

from . import datasets

from . import viz
