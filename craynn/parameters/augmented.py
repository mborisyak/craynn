from ..meta import derive
from .meta import Parameter
from .common import const_init

__all__ = [
  'scaled',
  'biased',
  'default_dependency_strategy'
]

def default_dependency_strategy(param):
  if param.properties.get('weights', False):
    return scaled(param, dependency_strategy=default_dependency_strategy)
  elif param.properties.get('biases', False):
    return biased(param, dependency_strategy=default_dependency_strategy)
  else:
    return param

class AugmentedParameter(Parameter):
  def __init__(self, param : Parameter, op, augment, dependency_strategy=default_dependency_strategy):
    self.op = op

    self.own_augments = [
      augment(param.shape)
      for _ in param.own_variables()
    ]

    self.augmented_dependencies = [
      dependency_strategy(dep_param)
      for dep_param in param.dependencies()
    ]

    self.param = param
    super(AugmentedParameter, self).__init__(param.shape, properties=param.properties, name=param.name)

  def get_augments(self):
    return self.own_augments + [
      augments
      for dep_param in self.augmented_dependencies
      for augments in getattr(dep_param, 'get_augments', lambda: [])()
    ]

  def own_variables(self):
    return self.param.own_variables()

  def dependencies(self):
    return self.own_augments + self.augmented_dependencies

  def get_output_for(self, vars, dependencies):
    n = len(vars)
    augments, deps = dependencies[:n], dependencies[n:]

    return self.param.get_output_for(
      vars=[
        self.op(var, augment())
        for augment, var in zip(augments, vars)
      ],
      dependencies=dependencies
    )

  def apply(self):
    for dep_param in self.augmented_dependencies:
      if isinstance(dep_param, AugmentedParameter):
        dep_param.apply()

    for var, augment in zip(self.param.own_variables(), self.own_augments):
      var.assign(self.op(var, augment()))

def _scale(var, augment):
  return var * augment

def _bias(var, augment):
  return var + augment

ScaledParameter = derive('ScaledParameter').based_on(AugmentedParameter).with_fixed(
  op=_scale,
  augment=lambda shape: const_init(value=1, augment=True)(shape=())
)

scaled = ScaledParameter

BiasedParameter = derive('BiasedParameter').based_on(AugmentedParameter).with_fixed(
  op=_bias,
  augment=lambda shape: const_init(value=0, augment=True)(shape=())
)

biased = BiasedParameter


