import tensorflow as tf

__all__ = [
  'mse', 'mae'
]

def mse(target, predictions, weights=None):
  if weights is None:
    return tf.reduce_mean((target - predictions) ** 2)
  else:
    return tf.reduce_mean(weights * (target - predictions) ** 2)

def mae(target, predictions, weights=None):
  if weights is None:
    return tf.reduce_mean(tf.abs(target - predictions))
  else:
    return tf.reduce_mean(weights * tf.abs(target - predictions))