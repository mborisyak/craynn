import os

from urllib.request import urlopen
from urllib.parse import urlparse, urljoin

from .meta import CachedComputation
from craynn.datasets.utils.common import *

__all__ = [
  'download',
  'locate'
]

def download_and_save(url, path, warn=True):
  if os.path.exists(path):
    raise IOError('Path %s already exists!' % path)

  if warn:
    import warnings
    warnings.warn('Downloading %s to %s'% (url, path))

  response = urlopen(url)
  data = response.read()

  with open(path, 'wb') as f:
    f.write(data)

  return path

def ensure_downloaded(root, *urls, root_url=None, warn=True):
  ensure_directory(root)
  results = []

  for _url in urls:
    if root_url is not None:
      url = urljoin(root_url, _url)
    else:
      url = _url

    path = os.path.join(root, os.path.basename(urlparse(url).path))
    results.append(path)

    if not os.path.exists(path):
      download_and_save(url, path, warn=warn)

  if len(results) == 1:
    return results[0]
  else:
    return tuple(results)

class Download(CachedComputation):
  def __init__(self, path, *urls, root_url=None):
    self.urls = urls
    self.path = path
    self.root_url = root_url

  def __call__(self, root=None):
    root = get_data_root(root)
    path = os.path.join(root, self.path)

    return ensure_downloaded(path, *self.urls, root_url=self.root_url)

download = Download

class Locate(CachedComputation):
  def __init__(self, *items):
    self.items = items

  def __call__(self, root=None):
    root = get_data_root(root)

    results = []

    for item in self.items:
      path = os.path.join(root, item)
      if not os.path.exists(path):
        raise FileNotFoundError('File %s not found' % (path, ))

      results.append(path)

    if len(results) == 1:
      return results[0]
    else:
      return results

locate = Locate