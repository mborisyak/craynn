__all__ = [
  'Cache', 'NoCache',
  'CachedComputation',
  'dataflow'
]

class Cache(object):
  def __rmatmul__(self, other):
    return GenericCachedComputation(other, self)

  def load(self, root=None):
    raise NotImplementedError

  def save(self, args, root=None):
    raise NotImplementedError

class NoCache(Cache):
  def __init__(self):
    pass

  def save(self, obj, root=None):
    pass

  def load(self, root=None):
    raise FileNotFoundError()

def as_cached_computation(obj):
  if isinstance(obj, CachedComputation):
    return obj
  elif isinstance(obj, Cache):
    return GenericCachedComputation(lambda *args: None, obj)
  elif callable(obj):
    return GenericCachedComputation(obj, NoCache())
  else:
    raise RuntimeError('%s is not understood.' % obj)

class CachedComputation(object):
  def __call__(self, root=None):
    raise NotImplementedError()

  def __rshift__(self, other):
    other = as_cached_computation(other)
    return GenericCachedComputation(other.f, other.cache, [self])

  def __add__(self, other):
    other = as_cached_computation(other)
    return CombinedCachedComputation(self, other)


class CombinedCachedComputation(object):
  def __init__(self, *ccs):
    self.cached_computations = ccs

  def __call__(self, root=None):
    result = tuple()
    for cc in self.cached_computations:
      r = cc(root)
      if isinstance(r, tuple):
        result += r
      elif isinstance(r, list):
        result += tuple(r)
      else:
        result += (r, )

    return result


class GenericCachedComputation(CachedComputation):
  def __init__(self, f, cache, dependencies=None):
    self.f = f
    self.cache = cache
    self.dependencies = dependencies if dependencies is not None else []

  def __call__(self, root=None):
    try:
      return self.cache.load(root)
    except (FileNotFoundError, NotImplementedError):
      args = [ dep(root) for dep in self.dependencies ]
      result = self.f(*args)
      self.cache.save(result, root)

      return result
    except KeyboardInterrupt:
      raise

  def __rrshift__(self, dependencies):
    if not (isinstance(dependencies, list) or isinstance(dependencies, tuple)):
      dependencies = [dependencies]

    dependencies = [as_cached_computation(obj) for obj in dependencies]

    return GenericCachedComputation(self.f, self.cache, dependencies)

def _dataflow(args):
  if isinstance(args, tuple):
    result = _dataflow(args[0])
    for arg in args[1:]:
      result = result >> _dataflow(arg)

    return result
  elif isinstance(args, list):
    return CombinedCachedComputation(args)
  else:
    return args

dataflow = lambda *args: _dataflow(args)

