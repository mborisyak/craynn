"""
This module is mostly for test purposes.
"""

import numpy as np

__all__ = [
  'two_gaussians_dataset',
  'radial_gaussians_dataset',
  'non_linear_gaussians_dataset'
]

from craynn.utils.data import onehot

def permute(*args):
  assert len(args) > 0

  indx = np.random.permutation(args[0].shape[0])
  return [ x[indx] for x in args ]

def labels(n_classes, n_samples):
  return onehot(
    np.repeat(np.arange(n_classes, dtype='float32'), n_samples)
  )

def binary_labels(n_samples):
  return np.concatenate([
    np.zeros(n_samples, dtype='float32'),
    np.ones(n_samples, dtype='float32')
  ], axis=0)

def two_gaussians_dataset(n_dim=2, n_samples=1024, n_samples_test=None):
  assert n_dim > 1

  if n_samples_test is None:
    n_samples_test = n_samples

  X_train = np.random.normal(size=(n_samples * 2, n_dim)).astype('float32')
  X_test = np.random.normal(size=(n_samples_test * 2, n_dim)).astype('float32')

  y_train = binary_labels(n_samples)
  y_test = binary_labels(n_samples_test)

  unit = np.ones(n_dim, dtype='float32')# / np.float32(np.sqrt(n_dim))

  X_train = X_train + y_train[:, None] * unit[None, :]
  X_test = X_test + y_test[:, None] * unit[None, :]

  X_train, y_train = permute(X_train, y_train)
  X_test, y_test = permute(X_test, y_test)

  return X_train, y_train, X_test, y_test


def radial_gaussians_dataset(n_classes=2, n_dim=2, r=1.0, n_samples=1024, n_samples_test=None):
  assert n_dim > 1

  if n_samples_test is None:
    n_samples_test = n_samples

  X_train = np.random.normal(size=(n_samples * n_classes, n_dim)).astype('float32')
  X_test = np.random.normal(size=(n_samples_test * n_classes, n_dim)).astype('float32')

  y_train = labels(n_classes, n_samples)
  y_test = labels(n_classes, n_samples_test)

  d_angle = 2 * np.pi / n_classes

  for i in range(n_classes):
    offset = np.array([r * np.cos(d_angle * i), r * np.sin(d_angle * i)])

    i1 = i * n_samples
    i2 = i1 + n_samples

    X_train[i1:i2, :2] += offset

    i1 = i * n_samples_test
    i2 = i1 + n_samples_test
    X_test[i1:i2, :2] += offset

  X_train, y_train = permute(X_train, y_train)
  X_test, y_test = permute(X_test, y_test)

  return X_train, y_train, X_test, y_test

def non_linear_gaussians_dataset(n_dim, r=1.0, n_samples=1024, n_samples_test=None):
  if n_samples_test is None:
    n_samples_test = n_samples

  gen = lambda n: np.concatenate([
    np.random.normal(size=(n, n_dim)).astype('float32'),
    (np.random.normal(size=(n, n_dim)) * 1.5 + r * np.ones(n_dim) / np.sqrt(n_dim)).astype('float32'),
  ], axis=0)

  X_train = gen(n_samples)
  X_test = gen(n_samples)

  y_train = labels(2, n_samples)
  y_test = labels(2, n_samples_test)

  X_train, y_train = permute(X_train, y_train)
  X_test, y_test = permute(X_test, y_test)

  return X_train, y_train, X_test, y_test







