import numpy as np

from ...utils import onehot
from ..utils import unpack_gz
from ..load import download

__all__ = [
  'download_mnist_train',
  'download_mnist_test',
  'download_mnist',

  'read_mnist',
]

ROOT_URL = 'http://yann.lecun.com/exdb/mnist/'

TRAIN_DATA = 'train-images-idx3-ubyte.gz'
TRAIN_LABELS = 'train-labels-idx1-ubyte.gz'

TEST_DATA = 't10k-images-idx3-ubyte.gz'
TEST_LABELS = 't10k-labels-idx1-ubyte.gz'

download_mnist_train = lambda path: download(path, TRAIN_DATA, TRAIN_LABELS, root_url=ROOT_URL)
download_mnist_test = lambda path: download(path, TEST_DATA, TEST_LABELS, root_url=ROOT_URL)
download_mnist = lambda path: download(path, TRAIN_DATA, TRAIN_LABELS, TEST_DATA, TEST_LABELS, root_url=ROOT_URL)

def _read_mist_pair(data_path, labels_path, one_hot=True, cast='float32'):
  from array import array
  import struct

  data_raw = unpack_gz(data_path)
  _, _, n_rows, n_cols = struct.unpack(">IIII", data_raw[:16])
  data = np.array(array("b", data_raw[16:]), dtype='uint8').reshape((-1, 1, n_rows, n_cols))

  labels_raw = unpack_gz(labels_path)
  labels = np.array(array("b", labels_raw[8:]), dtype='uint8')

  if one_hot:
    labels = onehot(labels, 10)

  if cast is not None:
    data = data.astype(cast)
    labels = labels.astype(cast)

  if np.dtype(cast).kind == 'f':
    data /= 255.0

  return data, labels

def read_mnist(pathes, one_hot=True, cast='float32'):
  assert len(pathes) % 2 == 0, 'Looks like label files are missing'

  return tuple([
    arr
    for i in range(len(pathes) // 2)
    for arr in _read_mist_pair(pathes[2 * i], pathes[2 * i + 1], one_hot=one_hot, cast=cast)
  ])