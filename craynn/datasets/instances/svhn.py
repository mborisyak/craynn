### based on https://github.com/thomalm/svhn-multi-digit/blob/master/unpacker.py

import os
import os.path as osp

import tarfile
import tempfile
import pickle
from io import BytesIO

from warnings import warn

import numpy as np

try:
  from PIL import Image
except ImportError as e:
  Image = None

from craynn.datasets.utils import *

__all__ = [
  'svhn',
  'extract_svhn_info',
  'svhn_stream'
]

ROOT_URL = 'http://ufldl.stanford.edu/housenumbers/'

def extract_svhn_info_from_archive(root):
  try:
    import h5py
  except ImportError as e:
    raise Exception('SVHN dataset loader requires h5py package.') from e


  path = osp.join(root, 'digitStruct.mat')
  with h5py.File(path, 'r') as f:
    bboxes = f['digitStruct']['bbox']
    names = f['digitStruct']['name']

    def read_property(item, name):
      prop = f[item][name]
      if len(prop) == 1:
        return [prop.value[0, 0]]
      else:
        return [f[prop.value[j].item()].value[0][0] for j in range(len(prop))]

    results = []
    for i in range(len(names)):
      item = bboxes[i].item()

      height = read_property(item, 'height')
      width = read_property(item, 'width')
      left = read_property(item, 'left')
      top = read_property(item, 'top')
      label = read_property(item, 'label')

      properties = [
        (int(l), int(t), int(w), int(h), y)
        for l, t, w, h, y in  zip(left, top, width, height, label)
      ]
      name = ''.join([chr(c[0]) for c in f[names[i][0]].value])

      results.append(
        (name, properties)
      )

    return results

def extract_svhn_info(path):
  return pickle_cached(
    osp.join(path, 'index.pickled'),
    lambda : extract_svhn_info_from_archive(path)
  )

def svhn_stream(dataset, root, verbose=False):
  try:
    from PIL import Image
  except ImportError as e:
    raise Exception('SVHN dataset loader requires PIL package.') from e

  assert dataset in ['train', 'test', 'extra']

  dataset_path = osp.join(root, dataset)

  def ensure_dataset():
    ensure_directory(dataset_path)

    filename = '%s.tar.gz' % (dataset,)

    ensure_downloaded(root, filename, root_url=ROOT_URL)
    with tarfile.open(osp.join(root, filename), 'r:gz') as f:
      f.extractall(root)

    #os.remove(osp.join(root, filename))

  try:
    index = extract_svhn_info(dataset_path)
  except FileNotFoundError:
    ensure_dataset()
    index = extract_svhn_info(dataset_path)
  except KeyboardInterrupt:
    raise
  except Exception as e:
    warn('Failed to load index file: %s' % (e, ))
    ensure_dataset()
    index = extract_svhn_info(dataset_path)

  if verbose:
    import tqdm
    progress = lambda x: tqdm.tqdm(x)
  else:
    progress = lambda x: x

  for name, props in progress(index):
    img_path = osp.join(dataset_path, name)
    with Image.open(img_path, 'r') as img:
      yield np.array(img), props

def svhn_name(target_shape, preserve_aspect, resample, group):
  if target_shape is None:
    return 'svhn-raw'
  else:
    shape_str = '%dx%d' % target_shape

    resample_str = {
      None: 'nearest',
      Image.NEAREST: 'nearest',
      Image.BILINEAR: 'bilinear',
      Image.BICUBIC: 'bicubic'
    }.get(resample, None)

    if resample_str is None:
      raise ValueError('Unknown resampling mode: %s' % (resample,))

    if preserve_aspect:
      mode = 'cropped'
    else:
      mode = 'stretched'

    if group:
      return 'svhn-%s-%s-%s-grouped' % (shape_str, mode, resample_str)
    else:
      return 'svhn-%s-%s-%s' % (shape_str, mode, resample_str)

def transform_image_crop(image, props, target_shape, resample):
  target_width, target_height = target_shape

  results = []

  for left, top, width, height, label in props:
    if target_width * height > width * target_height:
      current_height = height
      current_width = height * target_width // target_height
    else:
      current_width = width
      current_height = width * target_height // target_width

    center_x = left + width // 2
    center_y = top + height // 2

    current_left = center_x - current_width // 2
    current_top = center_y - current_height // 2

    if current_left < 0:
      current_left = 0

    if current_left + current_width >= image.shape[1]:
      current_left = image.shape[1] - current_width - 1

    if current_top < 0:
      current_top = 0

    if current_top >= image.shape[0]:
      current_top = image.shape[0] - current_height - 1

    from_x = current_top
    to_x = min(current_top + current_height, image.shape[0])

    from_y = current_left
    to_y = min(current_left + current_width, image.shape[1])

    cropped = Image.fromarray(
      image[from_x:to_x, from_y:to_y]
    ).resize(target_shape, resample=resample)
    results.append(np.array(cropped))

  return results

def transform_image_stretch(image, props, target_shape, resample):
  results = []

  for left, top, width, height, label in props:
    from_x = max(top, 0)
    to_x = min(top + height, image.shape[0])

    from_y = max(left, 0)
    to_y = min(left + width, image.shape[1])

    cropped = Image.fromarray(
      image[from_x:to_x, from_y:to_y]
    ).resize(target_shape, resample=resample)

    results.append(np.array(cropped))

  return results


def transform_image(image, props, target_shape, resample, preserve_aspect):
  if preserve_aspect:
    return transform_image_crop(image, props, target_shape, resample)
  else:
    return transform_image_stretch(image, props, target_shape, resample)


def svhn(root=None, extra=False, target_shape=None, preserve_aspect=True, group=False, resample=None, verbose=True):
  if resample is None:
    resample = Image.NEAREST

  root = get_data_root(root)

  original_root = osp.join(root, 'svhn')

  name = svhn_name(target_shape, preserve_aspect, resample, group)
  target_root = osp.join(root, name)
  ensure_directory(target_root)

  def get_dataset(dataset):
    stream = svhn_stream(dataset, original_root, verbose=verbose)

    if target_shape is not None:
      images = list()
      labels = list()

      for image, props in stream:
        batch = transform_image(image, props, target_shape, resample, preserve_aspect)
        batch_labels = [
          int(label) % 10 for _, _, _, _, label in props
        ]

        if group:
          images.append(np.array(batch))
          labels.append(np.array(batch_labels))
        else:
          images.extend(batch)
          labels.extend(batch_labels)

      if group:
        return images, labels
      else:
        return np.array(images), np.array(labels)
    else:
      images = list()
      properties = list()

      for image, props in stream:
        images.append(image)
        properties.append(props)

      return images, properties

  X_train, y_train = pickle_cached(
    osp.join(target_root, 'train.pickled'), lambda : get_dataset('train')
  )

  X_test, y_test = pickle_cached(
    osp.join(target_root, 'test.pickled'), lambda: get_dataset('test')
  )

  if extra:
    X_extra, y_extra = pickle_cached(
      osp.join(target_root, 'extra.pickled'), lambda: get_dataset('extra')
    )
    return X_train, y_train, X_test, y_test, X_extra, y_extra
  else:
    return X_train, y_train, X_test, y_test