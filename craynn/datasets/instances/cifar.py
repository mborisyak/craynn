import os.path as osp

import numpy as np

from ..utils import *
from ..load import ensure_downloaded

from craynn.utils import onehot

__all__ = [
  'cifar10', 'cifar100'
]

CIFAR_ROOT_URL = 'https://www.cs.toronto.edu/~kriz/'

CIFAR_10_PATH = 'cifar-10-python.tar.gz'
CIFAR_100_PATH = 'cifar-100-python.tar.gz'

CIFAR_10_ARCHIVE_PREFIX = 'cifar-10-batches-py'
CIFAR_100_ARCHIVE_PREFIX = 'cifar-100-python'


def _cifar(name, archive_url, train_files, test_files, labels_names, root=None, one_hot=True, cast='float32'):
  """
  Retrieves CIFAR-10 dataset. If necessary, downloads it.

  Parameters
  ----------
  root
    Data root directory. This function looks for instances in numpy format in `<root>/mnist`
    and in `<root>/mnist-original` for the original files.
  one_hot
    whatever to encode labels in one-hot format.
  cast
    target instances dtype.

  Returns
  -------
    train images, train labels, test images, test labels as numpy arrays.
  """

  root = osp.join(get_data_root(root), name)

  def load():
    f = np.load(osp.join(root, 'data.npz'))
    results = f['data_train'], f['labels_train'], f['data_test'], f['labels_test']
    f.close()

    return results

  try:
    X_train, y_train, X_test, y_test = load()
  except:
    ensure_directory(root)
    ensure_downloaded(root, archive_url, root_url=CIFAR_ROOT_URL)

    def extract_batch(f):
      import pickle
      d = pickle.load(f, encoding='bytes')
      imgs = d[b'data'].reshape((-1, 3, 32, 32))
      imgs = np.transpose(imgs, axes=(0, 2, 3, 1))
      labels = [ d[key] for key in labels_names ]
      return imgs, labels

    def read_data(f, files):
      imgs, labels = zip(*[
        extract_batch(f.extractfile(path)) for path in files
      ])

      imgs = np.concatenate(imgs, axis=0)
      labels = [np.concatenate(l, axis=0) for l in zip(*labels)]

      return imgs, labels

    import tarfile
    with tarfile.open(osp.join(root, archive_url), 'r:gz') as f:
      X_train, y_train = read_data(f, train_files)
      X_test, y_test = read_data(f, test_files)

    np.savez(
      osp.join(root, 'data.npz'),
      data_train = X_train,
      data_test = X_test,
      labels_train = y_train,
      labels_test = y_test
    )

  if one_hot:
    y_train = [ onehot(y) for y in y_train ]
    y_test = [ onehot(y) for y in y_test ]

  if cast is True:
    cast = 'float32'

  if cast is not None:
    X_train = X_train.astype(cast)
    X_test = X_test.astype(cast)
    y_train = [ y.astype(cast) for y in y_train ]
    y_test = [ y.astype(cast) for y in y_test ]

    if np.dtype(cast).kind == 'f':
      X_train /= 255.0
      X_test /= 255.0

  return X_train, y_train, X_test, y_test


def cifar10(root=None, one_hot=True, cast='float32'):
  train_files = [
    osp.join(CIFAR_10_ARCHIVE_PREFIX, 'data_batch_%d' % (i + 1, ))
    for i in range(5)
  ]
  test_files = [ osp.join(CIFAR_10_ARCHIVE_PREFIX, 'test_batch') ]

  X_train, y_train, X_test, y_test = _cifar(
    'cifar10', CIFAR_10_PATH,
    train_files=train_files, test_files=test_files, labels_names=[b'labels'],
    root=root, one_hot=one_hot, cast=cast
  )

  assert len(y_train) == 1
  assert len(y_test) == 1

  return X_train, y_train[0], X_test, y_test[0]

def cifar100(root=None, one_hot=True, cast='float32'):
  train_files = [osp.join(CIFAR_100_ARCHIVE_PREFIX, 'train')]
  test_files = [osp.join(CIFAR_100_ARCHIVE_PREFIX, 'test')]

  return _cifar(
    'cifar100', CIFAR_100_PATH,
    train_files=train_files, test_files=test_files,
    labels_names=[b'coarse_labels', b'fine_labels'],
    root=root, one_hot=one_hot, cast=cast
  )