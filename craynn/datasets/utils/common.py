import os.path as osp
import os

__all__ = [
  'get_data_root',
  'ensure_directory',
  'extract_tar',
  'unpack_gz',
]

DATA_ROOT_VARS = [
  'CRAYNN_DATA_ROOT',
  'DATA_ROOT'
]

def get_data_root(root=None):
  import os

  if root is not None:
    return root

  for data_root_var in DATA_ROOT_VARS:
    if data_root_var in os.environ:
      return os.environ[data_root_var]

  return osp.abspath('./')

def ensure_directory(path):
  if not osp.exists(path):
    os.makedirs(path)
  else:
    if not osp.isdir(path):
      raise Exception('%s is present but is not a directory!' % path)
    else:
      pass

def extract_tar(path, destination, mode='r:gz'):
  import tarfile

  with tarfile.open(path, mode) as f:
    f.extractall(destination)

def unpack_gz(path):
  import gzip

  with gzip.open(path, 'rb') as f:
    return f.read()

def unzip(path):
  import zipfile
  destination = os.path.dirname(path)

  with zipfile.ZipFile(path, mode='r') as zf:
    files = zf.namelist()
    if all([
      osp.exists(osp.join(destination, item))
      for item in files
    ]):
      pass
    else:
      zf.extractall(path=destination)

    return destination


