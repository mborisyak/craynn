import collections

import numpy as np
import tensorflow as tf

__all__ = [
  'VariableDataset', 'variable_dataset',

  'GroupedVariableDataset', 'grouped_variable_dataset'
]

class VariableDataset(object):
  def __init__(self, *shapes_or_arrays, dtypes='float32'):
    assert len(shapes_or_arrays) > 0

    shapes = [
      shape_or_array.shape if hasattr(shape_or_array, 'shape') else shape_or_array
      for shape_or_array in shapes_or_arrays
    ]

    general_shapes = [ (None, ) + shape[1:] for shape in shapes ]

    self.shapes = general_shapes

    if not isinstance(dtypes, collections.Iterable) or isinstance(dtypes, str):
      default_dtype = dtypes
      dtypes = []

      for shape_or_array in shapes_or_arrays:
        if hasattr(shape_or_array, 'dtype'):
          dtypes.append(shape_or_array.dtype)
        else:
          dtypes.append(default_dtype)

    self.vars = [
      tf.Variable(
        initial_value=tf.zeros(shape, dtype=dtype),
        dtype=dtype,
        validate_shape=False,
        shape=general_shape,
      ) for shape, general_shape, dtype in zip(shapes, general_shapes, dtypes)
    ]

    self._size = self.size()

    self.subset = SubsetConstructor(self)

  def size(self):
    return self.vars[0].shape[0]

  def symbolic_size(self):
    return tf.shape(self.vars[0])[0]

  @tf.function
  def assign(self, *data):
    return [
      var.assign(value)
      for var, value in zip(self.variables(), data)
    ]

  def __getitem__(self, item):
    return tuple(
      var[item]
      for var in self.variables()
    )

  def batch(self, batch_size):
    if batch_size is None:
      shape = ()
      selector = lambda x, indx: x[indx]
    else:
      shape = (batch_size, )
      selector = lambda x, indx: tf.gather(x, indx, axis=0)

    indx = tf.random.uniform(
      shape=shape,
      dtype=tf.int32,
      minval=0,
      maxval=self.symbolic_size()
    )
    return tuple(
      selector(v, indx)
      for v in self.vars
    )

  def batch_f(self, batch_size):
    if batch_size is None:
      shape = ()
      selector = lambda x, indx: x[indx]
    else:
      shape = (batch_size, )
      selector = lambda x, indx: tf.gather(x, indx, axis=0)

    @tf.function
    def g():
      indx = tf.random.uniform(
        shape=shape,
        dtype=tf.int32,
        minval=0,
        maxval=self.symbolic_size()
      )
      return tuple(
        selector(v, indx)
        for v in self.vars
      )

    return g

  def seq(self, batch_size=None):
    size = tf.shape(self.variables()[0])[0]

    if batch_size is not None:
      n_batches = size // batch_size + (1 if size % batch_size != 0 else 0)

      for i in range(n_batches):
        indx = slice(i * batch_size, min((i + 1) * batch_size, size))
        yield tuple([
          var[indx]
          for var in self.variables()
        ])
    else:
      for i in range(size):
        yield tuple([
          var[i]
          for var in self.variables()
        ])

  def eval(self, f, batch_size=None):
    size = tf.shape(self.variables()[0])[0]

    results = None

    make_buffer = lambda batch_results, bdim=1: tuple([
      np.ndarray(shape=(size,) + br.shape[bdim:], dtype=br.dtype.as_numpy_dtype)
      for br in batch_results
    ])

    if batch_size is not None:
      for i, batch in enumerate(self.seq(batch_size=batch_size)):
        indx = slice(i * batch_size, min((i + 1) * batch_size, size))
        batch_results = f(*batch)

        if results is None:
          results = make_buffer(batch_results)

        for br, r in zip(batch_results, results):
          r[indx] = br
    else:
      for i, batch in enumerate(self.seq(batch_size=batch_size)):
        batch_results = f(*batch)

        if results is None:
          results = make_buffer(batch_results, 0)

        for br, r in zip(batch_results, results):
          r[i] = br

    return results

  def variables(self):
    return tuple(self.vars)

def variable_dataset(*data):
  dataset = VariableDataset(*data)
  dataset.assign(*data)
  return dataset


class SubsetConstructor(object):
  def __init__(self, dataset):
    self.dataset = dataset

  def __getitem__(self, item):
    if isinstance(item, slice):
      return SliceSubset(self.dataset, item)

  def __call__(self, item):
    if isinstance(item, slice):
      return SliceSubset(self.dataset, item)

class SliceSubset(object):
  def __init__(self, dataset : VariableDataset, item : slice):
    self.dataset = dataset
    self.item = item

    if item.step is None:
      def indx_op(shape, start, stop):
        return tf.random.uniform(
          shape=shape,
          dtype=tf.int32,
          minval=start,
          maxval=stop,
        )
    else:
      def indx_op(shape, start, stop):
        delta = (stop - start) // item.step

        indx_ = tf.random.uniform(
          shape=shape,
          dtype=tf.int32,
          minval=0,
          maxval=delta,
        )

        return indx_ * item.step + start

    self._indx_op = indx_op

  def batch(self, batch_size):
    if batch_size is None:
      shape = ()
      selector = lambda x, indx: x[indx]
    else:
      shape = (batch_size,)
      selector = lambda x, indx: tf.gather(x, indx)

    if self.item.start is None:
      start = 0
    elif self.item.start < 0:
      start = self.dataset.symbolic_size() + self.item.start
    else:
      start = self.item.start

    if self.item.stop is None:
      stop = self.dataset.symbolic_size()
    elif self.item.stop < 0:
      stop = self.dataset.symbolic_size() + self.item.stop
    else:
      stop = self.item.stop

    indx = self._indx_op(shape, start, stop)

    return tuple(
      selector(v, indx)
      for v in self.variables()
    )

  def variables(self):
    return self.dataset.variables()

  def batch_f(self, batch_size):
    @tf.function(autograph=False)
    def f():
      return self.batch(batch_size)

    return f

def indexed_subset(session, dataset : VariableDataset, indx):
  subset = dataset.indexed_subset()
  indx = np.asarray(indx, dtype='float32')

  session.run(**subset.assign(indx))

  return subset


def _batch_matrix(groups):
  cum_groups = tf.cumsum(tf.concat([[0], groups], axis=0))
  n = tf.reduce_sum(groups)
  r = tf.range(n, dtype='int32')

  left = cum_groups[:-1][None, None, :]
  right = cum_groups[1:][None, None, :]

  I = r[:, None, None]
  J = r[None, :, None]

  cast = lambda x: tf.cast(x, tf.float32)

  G = tf.reduce_sum(
    cast(I >= left) * cast(J >= left) * cast(I < right) * cast(J < right),
    axis=2,
  )

  G = G / tf.reduce_sum(G, axis=0)[None, :]

  return G

class GroupedVariableDataset(VariableDataset):
  def __init__(self, *shapes_or_arrays, dtypes='float32'):
    self.group_indices_var = tf.Variable(
      initial_value=tf.zeros(shape=(0, 2), dtype='int32'),
      trainable=False, validate_shape=False,
      dtype='int32'
    )
    self.group_indices_placeholder  = tf.placeholder(
      shape=(None, 2), dtype='int32'
    )

    self.group_indices_assign_op = tf.assign(
      self.group_indices_var, self.group_indices_placeholder,
      validate_shape=False
    )
    
    super(GroupedVariableDataset, self).__init__(*shapes_or_arrays, dtypes=dtypes)

  def size(self):
    return tf.shape(self.group_indices_var)[0]

  def assign(self, *data, group_indices):
    return dict(
      fetches=[self._assign_op, self.group_indices_assign_op],
      feed_dict=dict([
        (p, d) for p, d in zip(self._placeholders, data)
      ] + [
        (self.group_indices_placeholder, group_indices)
      ])
    )

  def batch_f(self, batch_size=None, return_group_indices=False):
    if batch_size is None or batch_size == 1:
      group_index = tf.random_uniform(shape=tuple(), dtype='int32', minval=0, maxval=self._size)
      group_range = self.group_indices_var[group_index]
      result = [
        v[group_range[0]:group_range[1]] for v in self.vars
      ]

      if return_group_indices:
        indx = tf.zeros(shape=group_range[1] - group_range[0], dtype='int32')
        return result, indx
      else:
        return result
    else:
      group_indices = tf.random_uniform(shape=(batch_size, ), dtype='int32', minval=0, maxval=self._size)
      group_ranges = tf.gather(
        self.group_indices_var,
        group_indices
      )
      result = [
        tf.concat([
          tf.gather(v, tf.range(group_ranges[i, 0], group_ranges[i, 1], dtype='int32'))
          for i in range(batch_size)
        ], axis=0)
        for v in self.vars
      ]

      if return_group_indices:
        indx = tf.concat([
          tf.ones(shape=group_ranges[i, 1] - group_ranges[i, 0], dtype='int32') * i
          for i in range(batch_size)
        ], axis=0)
        return result, indx
      else:
        return result

  def variables(self):
    return tuple(self.vars)


def _splits_to_groups(splits):
  groups = np.ndarray((splits.shape[0] - 1, 2), dtype='int32')
  groups[:, 0] = splits[:-1]
  groups[:, 1] = splits[1:]
  return groups

def _sizes_to_groups(sizes):
  groups = np.ndarray((len(sizes), 2), dtype='int32')
  c = np.cumsum(sizes, dtype='int32')

  groups[0, 0] = 0
  groups[1:, 0] = c[:-1]
  groups[:, 1] = c

  return groups

def grouped_variable_dataset(session, *data, group_indices=None, unpack=False):
  """
  Creates and fills `GroupedVariableDataset`.

  Can be used in 3 cases:
    - `data` is a single list of groups (`unpack` flag must be set), each possibly containing multiple arrays,
      e.g. [ (X_1, y_1), (X_2, y_2), ...];

    - `data` is multiple aligned lists of groups,
      e.g. [X_1, X_2, ...], [y_1, y_2, ...];

    - `data` is a single or multiple contiguous arrays, `group_indices` must be set in this case to form groups.

  `group_indices` is recognised in 2 formats:
    - list of group sizes, e.g. [2, 3, 2, 5, 4];
    - list of group indices, e.g. [ (1, 2, 3), (0, 4), (7, 8), ... ]

  """
  if all([ hasattr(d, 'shape') for d in data ]):
    assert group_indices is not None

    if isinstance(group_indices[0], int):
      group_indices = _sizes_to_groups(group_indices)
    else:
      ### assuming list of group indices
      order_index = np.array([
        indx for group in group_indices for indx in group
      ], dtype='int64')

      data = tuple([d[order_index] for d in data])
      group_indices = _sizes_to_groups([ len(group) for group in group_indices ])
  else:
    if unpack:
      assert len(data) == 1, 'Multiple `*data` arguments and `unpack` is set'
      assert group_indices is None, '`unpack` assumes data in format that does not require group_indices'

      data = data[0]
      data = list(zip(*data))

    ### assuming data as multiple lists of groups
    group_indices = _sizes_to_groups([
      d.shape[0] for d in data[0]
    ])

    data = [
      np.concatenate(d, axis=0)
      for d in data
    ]

  dataset = GroupedVariableDataset(*data)
  session.run(**dataset.assign(*data, group_indices=group_indices))
  return dataset


