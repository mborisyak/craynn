import numpy as np

__all__ = [
  'random', 'inf_random', 'random_seq', 'seq', 'inf_random_seq'
]

def _get_size(n_samples):
  if hasattr(n_samples, 'shape') and len(n_samples.shape) > 0:
    return n_samples.shape[0]
  else:
    try:
      return len(n_samples)
    except:
      return n_samples


def random(n_samples, batch_size=None, n_batches=None, replace=True, priors=None, seed=None):
  bs = 1 if batch_size is None else batch_size

  rng = np.random.RandomState(seed)

  n_samples = _get_size(n_samples)

  if n_batches is None:
    n_batches = n_samples // bs

  for i in range(n_batches):
    yield rng.choice(n_samples, size=batch_size, replace=replace, p=priors)

def inf_random(n_samples, batch_size=None, replace=True, priors=None, seed=None):
  rng = np.random.RandomState(seed)
  n_samples = _get_size(n_samples)

  while True:
      yield rng.choice(n_samples, size=batch_size, replace=replace, p=priors)

def seq(n_samples, batch_size=None):
  n_samples = _get_size(n_samples)
  indx = np.arange(n_samples)

  if batch_size is None:
    for i in indx:
      yield i

  else:
    n_batches = n_samples // batch_size + (1 if n_samples % batch_size != 0 else 0)

    for i in range(n_batches):
      i_from = i * batch_size
      i_to = i_from + batch_size
      yield indx[i_from:i_to]

def random_seq(n_samples, batch_size=None, allow_smaller=False, seed=None):
  rng = np.random.RandomState(seed)
  n_samples = _get_size(n_samples)
  indx = rng.permutation(n_samples)

  if batch_size is None:
    for i in indx:
      yield i
  else:
    n_batches = n_samples // batch_size + (1 if (n_samples % bs != 0) and allow_smaller else 0)

    for i in range(n_batches):
      i_from = i * batch_size
      i_to = i_from + batch_size
      yield indx[i_from:i_to]

def inf_random_seq(n_samples, batch_size=None, allow_smaller=False, seed=None):
  rng = np.random.RandomState(seed)
  n_samples = _get_size(n_samples)

  if batch_size is not None:
    n_batches = n_samples // batch_size + (1 if (n_samples % batch_size != 0) and allow_smaller else 0)

    indx = rng.permutation(n_samples)
    for i in range(n_batches):
      i_from = i * batch_size
      i_to = i_from + batch_size
      yield indx[i_from:i_to]
  else:
    while True:
      for i in rng.permutation(n_samples):
        yield i