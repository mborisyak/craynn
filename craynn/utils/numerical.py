import tensorflow as tf

__all__ = [
  'log_sum_exp'
]

def log_sum_exp(*xs):
  from functools import reduce

  max_x = reduce(lambda a, b: tf.maximum(a, b), xs)
  normalized_xs = [ x - max_x for x in xs ]
  exped = [ tf.exp(x) for x in normalized_xs ]
  sumed = sum(exped)
  return tf.log(sumed) + max_x