import numpy as np
import tensorflow as tf

from .. import layers
from ..parameters import FreeParameter

__all__ = [
  'normalize_weights'
]


def normalize_weights(network, batch_f):
  for layer in network.layers():
    weights = layers.get_parameters(layer, weights=True)

    if len(weights) == 0:
      continue

    vars = [
      var
      for weight in weights
      if isinstance(weight, FreeParameter)

      for var in weight.variables()
    ]

    output = layers.get_output(layer, substitutes=dict(zip(network.inputs, batch_f())))
    std = np.std(output)

    scale = np.power(1 / std, 1 / len(vars))

    for var in vars:
      var.assign(var * scale)

def __normalize_output(
  network, batch_f, optimizer=None,
  n_iters=16 * 1024,
  target_mean=0.0, target_variance=1.0,
  strategy=None
):
  augmented_parameters = {
    layer : [
      strategy(param)
      for param in layers.get_parameters(layer)
    ]
    for layer in network.layers()
  }

  augments = [
    augments()
    for layer in augmented_parameters
    for param in augmented_parameters[layer]
    for augments in param.get_augments()
  ]

  def layer_output(layer, *inputs, **modes):
    param_values = [
      param()
      for param in augmented_parameters[layer]
    ]

    return apply_with_kwargs(layer.get_output_for, *param_values, *inputs, **modes)

  get_output = layers.reduce_graph(lambda layer: lambda *inputs, **modes: layer_output(layer, *inputs, **modes), strict=True)

  @tf.function(autograph=False)
  def call():
    args = batch_f()
    result, = get_output(network.outputs, substitutes=dict(zip(network.inputs, args)))
    return result

  @tf.function
  def target():
    result = call()
    mean = tf.reduce_mean(result)
    variance = tf.reduce_mean((result - mean) ** 2)

    return (mean - target_mean) ** 2 + (variance - target_variance) ** 2

  opt = optimizer(target, augments)

  for _ in range(n_iters):
    opt()

  for layer in augmented_parameters:
    for param in augmented_parameters[layer]:
      param.apply()





